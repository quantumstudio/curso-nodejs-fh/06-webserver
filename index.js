const express = require('express');
const hbs = require('hbs');

// Load env variables
require('dotenv').config();

const app = express();

// Handlebars config
app.set('view engine', 'hbs');
hbs.registerPartials(`${__dirname}/views/partials`, (error) => {
  console.error('Error al registrar partials para hbs: ', error);
});

// Middleware (Serve static content)
app.use(express.static(`${__dirname}/public`));

app.get('/', (req, res) => {
  res.render('home', {
    name: 'Aarón RJ',
    title: 'Curso de Node'
  });
});

app.get('/generic', (req, res) => {
  res.render('generic', {
    name: 'Aarón RJ',
    title: 'Generic - Curso de Node'
  });
});

app.get('/elements', (req, res) => {
  res.render('elements', {
    name: 'Aarón RJ',
    title: 'Elements - Curso de Node'
  });
});

// app.get('*', (req, res) => {
//   res.sendFile(`${__dirname}/public/404.html`);
// })

app.listen(process.env.PORT, () => {
  console.log(`Servidor conectado desde el puerto ${process.env.PORT}`);
});
